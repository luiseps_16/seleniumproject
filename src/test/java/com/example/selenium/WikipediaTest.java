package com.example.selenium;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WikipediaTest {
	
	private WebDriver driver;

	@Before
	public void driverInicialization() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@Test
	public void test() {
		
		String wordTosearch = "Colombia";
		driver.get("https://es.wikipedia.org");
		WebElement fieldSearch = driver.findElement(By.id("searchInput"));
		WebElement buttonSearch = driver.findElement(By.id("searchButton"));
				
		fieldSearch.sendKeys(wordTosearch);
		buttonSearch.click();
		WebElement title = driver.findElement(By.id("firstHeading"));
		assertEquals(wordTosearch, title.getText().toString());
		
	}
	
	@After
	public void closeDriver() {
		driver.quit();
	}
}
